<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
error_log('jwt.php');
return [
 
    'secret' => env('JWT_SECRET', 'AIlXkDvbMdYMUgR9M3Su2zsx61hqLpY96nQ4zFkuqowFae29y0on9qu6PeEmkj0d'),
  
    'ttl' => 60 * 24 * 60, // 60 Days
 
    'refresh_ttl' => 20160,
 
    'algo' => 'HS256',
 
    'user' => 'App\User',
  
    'identifier' => 'id',

    'required_claims' => ['iss', 'iat', 'exp', 'nbf', 'sub', 'jti'],
    
    'blacklist_enabled' => env('JWT_BLACKLIST_ENABLED', true),

    'providers' => [
        /*
        |--------------------------------------------------------------------------
        | User Provider
        |--------------------------------------------------------------------------
        |
        | Specify the provider that is used to find the user based
        | on the subject claim
        |
        */
        'user' => 'Tymon\JWTAuth\Providers\User\EloquentUserAdapter',
      
        // 'jwt' => 'Tymon\JWTAuth\Providers\JWT\NamshiAdapter',
        'jwt' => 'Tymon\JWTAuth\Providers\JWT\Namshi',
     
        'auth' => 'Tymon\JWTAuth\Providers\Auth\Illuminate',
   
        'storage' => 'Tymon\JWTAuth\Providers\Storage\Illuminate',
    ],
];