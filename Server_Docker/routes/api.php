<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//     Route::get('user', 'UserController@index');
//     Route::match(['put', 'patch'], 'user', 'UserController@update');

//     Route::get('profiles/{user}', 'ProfileController@show');
//     Route::post('profiles/{user}/follow', 'ProfileController@follow');
//     Route::delete('profiles/{user}/follow', 'ProfileController@unFollow');
Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'v1'], function() {
    Route::get('Rocket', 'APIControllers\RocketController@index');
 
});


Route::group(['prefix' => 'v1'], function() {
    // Route::get('Flights', 'APIControllers\FlightsController@index');
    // Route::get('Flights/{id}', 'APIControllers\FlightsController@FindOne');
    Route::resource('Flights', 'APIControllers\FlightsController')->except(['show']);
    Route::get('Comments/{id}', 'APIControllers\CommentsController@getByFlightId');

    Route::get('Flights/{slug}', 'APIControllers\FlightsController@getBySlug');
    Route::resource('Flights', 'APIControllers\FlightsController' );
    Route::resource('Comments', 'APIControllers\CommentsController');

    Route::get('tags', function () {
        error_log('hola');
        return json_encode(array('hola', 'pipo'));
    });
    Route::get('/redis', function(){
        
     
        
        return Redis::get("last.visited");
    });
    Route::get('social', 'APIControllers\AuthController@showLoginPage');
    Route::post('users/login', 'APIControllers\AuthController@login');
    Route::post('users/', 'APIControllers\AuthController@register');
 
});

