<?php

namespace App\ExtraThings\transformers;
use Auth;
class CommentsTransformer extends Transformer{

    protected $resourceName = 'comment';

    public function transform($data)
    {
        return [
            'id'        => $data['id'],
            'body'      => $data['body'],
            'createdAt' => $data['created_at']->toAtomString(),
            'updatedAt' => $data['updated_at']->toAtomString(),
            'flight_id' => $data['flight_id'],
            'user_id' => $data['user_id'],
            'author' => [
                'username'  => Auth::user()->username,
                'bio'       => Auth::user()->bio,
                'image'     =>Auth::user()->image,
            ]
        ];
    }
}