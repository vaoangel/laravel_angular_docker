<?php

namespace App\ExtraThings\transformers;

use Auth;

class FlightsTransformer extends Transformer{


        protected $resourceName = 'flights';

        public function transform($data){
            return[
                'slug'              => $data['slug'],
                'nombre'              => $data['nombre'],
                'user_id'           => $data['user_id'],
                'pais'             => $data['pais'],
                'valoracion'       => $data['valoracion'],
                'hoteles'              => $data['hoteles'],
                'createdAt'         => $data['created_at']->toAtomString(),
                'updatedAt'         => $data['updated_at']->toAtomString(),
                'author' => [
                    'username'  => Auth::user()->username,
                    'bio'       => Auth::user()->bio,
                    'image'     =>Auth::user()->image,
                ]


            ];
        }
}