<?php

namespace App\ExtraThings\transformers;
use Auth;

class UserTransformer extends Transformer
{
    protected $resourceName = 'user';

    public function transform($data)
    {
        return [
            'id' => Auth::user()->id,
            'email'     => $data['email'],
            'token'     => $data['token'],
            'userSocial'=> $data['userSocial'],
            'username'  => $data['username'],
            'bio'       => $data['bio'],
            'image'     => $data['image'],
        ];
    }
}