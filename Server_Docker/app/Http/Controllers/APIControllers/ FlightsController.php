<?php

namespace App\Http\Controllers\APIControllers;


use App\Flights;
use App\Http\Requests\Api\CreateFlight;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Redis;
use App\ExtraThings\transformers\FlightsTransformer;
class FlightsController extends ApiController
{
   

    public function __construct(FlightsTransformer $transformer)
    {
            

        $this->transformer = $transformer;
         $this->middleware('jwt.auth')->except(['index', 'FindOne', 'getBySlug', 'show']);

    }

    

    public function index()
    {
        
        // $result = Flights::getList();
        
         return Flights::all();
    }

    public function FindOne($id){
        
        // $result = Flights::FindOne($id);
        
        // return response($result);
    }

    public function getBySlug($slug){
        $newPush = Redis::get("last.visited");
         $newPush = json_decode($newPush);
         array_push($newPush, $slug);

    //    array_push($redis, $slug);

       $test  = json_encode($newPush);
       Redis::set("last.visited", $test);

         return Flights::query()->where('slug', $slug)->get();
    }

  
    

    public function store(CreateFlight $request)
    {
        

        
       
         $flight = Flights::create([
            'nombre' => $request->input('flight.nombre'),
            'user_id' => Auth::user()->id,

            'pais' => $request->input('flight.pais'),
            'slug' => $this->generateSlug($request->input('flight.nombre')),
            'valoracion' => $request->input('flight.valoracion'),
            'hoteles' => $request->input('flight.hoteles'),


           ]);
          return $this->respondWithTransformer($flight);
    }
  
    /**
     * Display the specified resource.
     *
     * @param  \App\Flights  $flights
     * @return \Illuminate\Http\Response
     */
    public function show(Flights $Flight)
    {
        return $Flight;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Flights  $flights
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Flights $flights)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Flights  $flights
     * @return \Illuminate\Http\Response
     */
    public function destroy(Flights $Flight)
    {
      
     $Flight -> delete();

    

    }
}


