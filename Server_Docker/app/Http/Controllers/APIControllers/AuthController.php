<?php

// namespace App\Http\Controllers\APIControllers;

// use App\User;
// use Illuminate\Http\Request;
// use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Validator;
// use Illuminate\Contracts\Auth\Guard;
// use Illuminate\Foundation\Auth\RegistersUsers;

// class AuthController extends Controller
// {
//     /**
//      * Display a listing of the resource.
//      *
//      * @return \Illuminate\Http\Response
//      */
     
//      use RegistersUsers;
        
//     public function register(Guard $auth, Request $request) {
        
         
//         $fields = ['email', 'password', 'username'];
//          $credentials = $request->only($fields);
//         // // grab credentials from the request
        
        
//          $validator = Validator::make(
//              $credentials,
//              [
//                 'username' => 'required',
//                  'email' => 'required',
//                  'password' => 'required',
//              ]
//              );
//          if ($validator->fails())
//          {
//              return response($validator->messages());
//          }
//         //  print_r($validator);

//         $result = User::create([
//             'username' => $credentials['username'],
//             'email' => $credentials['email'],
//             'password' => bcrypt($credentials['password']),
//         ]);
        
//         $result['token'] = $this->tokenFromUser($result['id']);        

//         return response($result->only(['email', 'token']));
//         // return "request";
//     }
    
    
//     protected function login(Request $request) {
        
//         auth()->shouldUse('api');
//         // grab credentials from the request
//         $credentials = $request->only('email', 'password');

//         if (auth()->attempt($credentials)) {
//             $result['token'] = auth()->issue();
//             $result['email'] = $credentials['email'];
//            return response($result);
//         }
    
//         return response(['Invalid Credentials']);
//     }
    
//     public function tokenFromUser($id)
//     {
//         // generating a token from a given user.
//         $user = User::find($id);
    
//         auth()->shouldUse('api');
//         // logs in the user
//         auth()->login($user);
    
//         // get and return a new token
//         $token = auth()->issue();
    
//         return $token;
//     }
// }


namespace App\Http\Controllers\APIControllers;
use Auth;
use App\User;
 use App\Http\Requests\Api\LoginUser;
use App\Http\Requests\Api\RegisterUser;
use App\ExtraThings\transformers\UserTransformer;

class AuthController extends ApiController
{
    
    public function __construct(UserTransformer $transformer)
    {
        $this->transformer = $transformer;
    }


    /**
     * Register a new user and return the user if successful.
     *
     * @param RegisterUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterUser $request)
    {
        // print_r($request->input('user.username'));
        //  print_r($request);
        $user = User::create([
            'username' => $request->input('user.username'),
            'email' => $request->input('user.email'),
            'password' => $request->input('user.password'),
        ]);

        // print_r($user);

          return $this->respondWithTransformer($user);
    }

    public function login(LoginUser $request)
    {
        $credentials = $request->only('user.email', 'user.password');
       
         $credentials = $credentials['user'];
        // return $credentials;

        if (! Auth::once($credentials)) {
            return $this->respondFailedLogin();
        }

          return $this->respondWithTransformer(auth()->user());
    }

    
}