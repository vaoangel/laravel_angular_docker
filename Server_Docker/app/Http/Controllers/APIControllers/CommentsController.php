<?php


namespace App\Http\Controllers\APIControllers;

use App\ExtraThings\transformers\CommentsTransformer;
use App\Flights;
use App\Comments;
use App\Http\Requests\Api\CreateComment;
use App\Http\Requests\Api\DeleteComment;
use App\User;
use Auth;
use Symfony\Component\HttpFoundation\Request;

class CommentsController extends ApiController{
    public function __construct(CommentsTransformer $transformer)
    {
        $this->transformer = $transformer;
          $this->middleware('jwt.auth')->except(['getByFlightId']);
    }

    public function index(Flights $flight)
    {
        $comments = $flight->comments()->get();

        return $this->respondWithTransformer($comments);
    }

    public function store(CreateComment $request)
    {
        // return $request;
        $comment = Comments::create([
            'body' => $request->input('comment.body'),
            'flight_id' => $request->input('comment.flight_id'),

            'user_id' => Auth::user()->id,

        ]);

        return $this->respondWithTransformer($comment);
    }

    public function destroy(DeleteComment $request, $flight, Comments $comment)
    {
        $comment->delete();

        return $this->respondSuccess();
    }

    public function getByFlightId($id){
        return Comments::query()->where('flight_id', $id)->get();
    }
}