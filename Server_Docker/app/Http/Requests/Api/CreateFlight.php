<?php

namespace App\Http\Requests\Api;


class CreateFlight extends ApiRequest{
    public function validationData()
    {
        return $this->get('flight') ?: [];
    }

    public function rules(){

        return [
            'nombre' => 'required',
            'pais' => 'required',
            'valoracion' => 'required'
        ];
    }
}