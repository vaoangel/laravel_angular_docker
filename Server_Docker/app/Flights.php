<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Flights extends Model
{

    protected $table = "flights";
    protected $fillable = [
        'nombre', 'pais', 'valoracion', 'hoteles','slug','user_id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function comments()
    {
        return $this->hasMany(Comment::class)->latest();
    }
}