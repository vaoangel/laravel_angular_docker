<?php

use Illuminate\Database\Seeder;

class FlightsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('flights')->truncate();

        $parentId_1 = DB::table('flights')->insertGetId(
            array(
                'nombre' => 'Iberia',
                'pais' => 'España',
                'valoracion' => '8d',
                'hoteles' => 'Estacio'
                )
        );

        $parentId_2 = DB::table('flights')->insertGetId(
            array(
                'nombre' => 'Iberia2',
                'pais' => 'España2',
                'valoracion' => '8d2',
                'hoteles' => 'Estacio2'
                )
        );


    }
}


