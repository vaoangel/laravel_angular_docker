<?php


namespace App\Http\Controllers\APIControllers;


use App\Flights;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class FlightsController extends Controller
{
    function __construct()
    {
        
    }

    public function index()
    {
        
        // $result = Flights::getList();
        
         return Flights::all();
    }

    public function FindOne($id){
        // $result = Flights::FindOne($id);
        
        // return response($result);
    }


    public function store(Request $request)
    {
       
         $flight = new Flights($request-> flights);
         $flight-> save();
        return $flight;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Flights  $flights
     * @return \Illuminate\Http\Response
     */
    public function show(Flights $Flight)
    {
        return $Flight;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Flights  $flights
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Flights $flights)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Flights  $flights
     * @return \Illuminate\Http\Response
     */
    public function destroy(Flights $Flight)
    {
      
     $Flight -> delete();

    

    }
}


