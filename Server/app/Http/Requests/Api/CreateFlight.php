<?php

namespace App\Http\Requests\Api;

class CreateFlight extends ApiRequest
{
       /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return $this->get('flights') ?: [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|max:255',
            'pais' => 'required|string|max:255',
            'valoracion' => 'required|string',
            'hoteles' => 'required|string',
        ];
    }
}