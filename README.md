# Laravel_Angular_Docker

## Install laravel server with docker

cd Server_docker;

chmod 777 -R Server_docker

modify the database files to your respective models and db tables;

kill apache
kill mysql

kill port 8080 : 80

sudo docker-compose up --build

### Inside the docker containers

connect to the mysql container: docker-compose exec 'the name of the db container' bash

mysql -u root -p;

show databases;

### if your database isn't created , create it

create database 'db_name';

GRANT ALL ON laravel.* TO 'your user'@'%' IDENTIFIED BY 'your_laravel_db_password';

FLUSH PRIVILEGES;

Exit;

Exit

docker-compose exec app php artisan migrate

docker-compose exec app php artisan tinker

\DB::table('migrations')->get();


# Code

- Mysql Laravel connected
- Laravel requests with transformers
- JwtAuth Middleware
- Redis on laravel 
- Relationships Laravel Mysql
- Routing with Resources on Laravel
- Redis, Laravel, Mysql  working on docker
- Migrations tables with relationships 
- List details on Angular
- Last visited component used with Redis db
- Last visited component with Angular/Redux
- CRUD of Flights
- CRUD of Comments inside the flights
- Sing in and Sing up