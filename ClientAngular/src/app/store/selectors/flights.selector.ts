import { createSelector } from '@ngrx/store';

import { IAppState } from '../state/app.state';

import {IFlightsState} from '../state/flights.state';

const selectFlights = (state: IAppState) => state.flights;

export const selectFlightsList = createSelector(
    selectFlights,
    (state: IFlightsState) => state.flights
)

export const selectSelectedFlight = createSelector(
    selectFlights,
    (state: IFlightsState) => state.SelectedFlight

)
