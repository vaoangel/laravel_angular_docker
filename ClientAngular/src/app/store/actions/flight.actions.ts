import { Action } from '@ngrx/store';
import { IFlight } from '../../core/models/flight.interface';

export enum EFlightActions {
    GetFlights = '[IFlight] Get Flights',
    GetFlightsSuccess = '[IFlight] Get Flights Success',
    GetFlight = '[IFlight] Get Flight',

    GetFlightSuccess = '[IFlight] Get Flight Success',



}

export class GetFlights implements Action {
    public readonly type = EFlightActions.GetFlights;
}

export class GetFlightsSuccess implements Action {
    public readonly type = EFlightActions.GetFlightsSuccess;
    constructor(public payload: any[]){}
}  

export class GetFlight implements Action {
    public readonly type = EFlightActions.GetFlight;
    constructor(public payload: number) {}
  }

export class GetFlightSuccess implements Action {
    public readonly type = EFlightActions.GetFlightSuccess;
    constructor(public payload: any){}
} 



export type FlightsActions = GetFlights | GetFlightsSuccess | GetFlight | GetFlightSuccess;
