import {EFlightActions } from '../actions/flight.actions';

import { FlightsActions } from '../actions/flight.actions';
import { IFlightsState , initialFlightsState} from '../state/flights.state';


export const FlightsReducers = (
    state: IFlightsState = initialFlightsState,
    action: FlightsActions
    ): IFlightsState => {
        switch (action.type) {
            case EFlightActions.GetFlightsSuccess: {
                return{
                    ...state,
                    flights: action.payload,
                };
            }
             
        
            default:
                return state;
                
        }
    };


