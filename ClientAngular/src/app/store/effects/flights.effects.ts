import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { of } from 'rxjs';
import { switchMap, map, withLatestFrom } from 'rxjs/operators';
import { IFlightHttp } from '../../core/models/http-flight';

import { IAppState } from '../state/app.state';

import {
    GetFlights,
    GetFlight,
    GetFlightSuccess,
    EFlightActions,
    GetFlightsSuccess
} from '../actions/flight.actions';

import { FlightsService } from '../../core/services/flights.service';

import {selectFlightsList} from '../selectors/flights.selector';

@Injectable()

export class FlightsEffects {
    @Effect()
    getFlight$ = this._actions$.pipe(
        ofType<GetFlight>(EFlightActions.GetFlight),
        map(action => action.payload), 
        withLatestFrom(this._store.pipe(select(selectFlightsList))),
        switchMap(([id , flights])=> {
            const selectedFlight = flights.filter(flight => flight.id === +id)[0];
            return of(new GetFlightSuccess(selectedFlight));
        })
    )

    @Effect()
    getFlights$ = this._actions$.pipe(
      ofType<GetFlights>(EFlightActions.GetFlights),
      switchMap(() => this._FlightsService.getRedis()),
      switchMap((flightHttp: any) => of(new GetFlightsSuccess(flightHttp)))

    );

  constructor(
    private _FlightsService: FlightsService,
    private _actions$: Actions,
    private _store: Store<IAppState>
  ) {}

}
 