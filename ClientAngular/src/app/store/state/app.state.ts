import { RouterReducerState } from '@ngrx/router-store';

// import { IUserState, initialUserState } from './user.state';
// import { initialConfigState, IConfigState } from './config.state';

import { IFlightsState , initialFlightsState} from './flights.state';


export interface IAppState {
  router?: RouterReducerState;
  flights: IFlightsState;
}

export const initialAppState: IAppState = {
  flights: initialFlightsState
};

export function getInitialState(): IAppState {
  return initialAppState;
}
