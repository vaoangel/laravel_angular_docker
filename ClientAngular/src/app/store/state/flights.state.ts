import { IFlight } from '../../core/models/flight.interface';


export interface IFlightsState {
    flights: IFlight[];
    SelectedFlight: IFlight;
}

export const initialFlightsState: IFlightsState = {
    flights: null,
    SelectedFlight:null
} 