import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IFlight } from '../models/flight.interface';

@Injectable({
  providedIn: 'root'
})
export class FlightsService {

  constructor(
    private apiService : ApiService
  ) { }



  getAll(): Observable<any> {
    return this.apiService.get('/Flights')
    .pipe(map(data => {
      return data;
    }))
  }

  getBySlug(slug): Observable<any> {
    return this.apiService.get('/Flights/'+slug).pipe(map(data => {return data}))
  }

  addOne(data){
    return this.apiService.post('/Flights/', {flight: data[0]}).pipe(map(data => {return data}))
  
  }

  getRedis(){
    return this.apiService.get('/redis').pipe(map(data => {return data}))
  }

  deleteOne(id){
    console.log("paco"+id)
    return this.apiService.delete('/Flights/'+id).pipe(map(data => {return data}));
  }

 
}
