export interface IFlight {
    id: number;
    user_id: number;
    nombre: string;
    pais: string;
    valoracion: string;
    hoteles: string;
    slug: string;
    created_at: string;
    updated_at: string;
  }