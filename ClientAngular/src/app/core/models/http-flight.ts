import {IFlight} from './flight.interface';

export interface IFlightHttp {
    flights: IFlight[];
  }