import { Component, OnInit, Input, Output, EventEmitter, NgModule } from '@angular/core';

import {GetFlights} from '../../store/actions/flight.actions';
import { Store, select } from '@ngrx/store';
import { IFlight } from '../../core/models/flight.interface';

import { IAppState } from '../../store/state/app.state';
import { Router } from '@angular/router';
import {selectFlightsList} from '../../store/selectors/flights.selector';

@Component({
  selector: 'app-redux',
  templateUrl: './redux.component.html',
  styleUrls: ['./redux.component.css']
})
export class ReduxComponent implements OnInit {
  @Input("flights")
flights: IFlight[];
@Output()
flightSelected: EventEmitter<number> = new EventEmitter();

flights$ = this._store.pipe(select(selectFlightsList))

constructor(private _store: Store<IAppState>, private _router: Router) {}

  ngOnInit() {
  this._store.dispatch(new GetFlights());

  
 
  }

  navigateToFlight(id: number) {
    this.flightSelected.emit(id);
  }
 
}
