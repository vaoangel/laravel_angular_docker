import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlightComponent } from './flight.component';
import { FlightResolver } from './flight-resolver.service';

const routes: Routes = [
    {
        path: ':slug',
        component: FlightComponent,
        resolve: {
            flight: FlightResolver
          }
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })


  export class FlightRoutingModule {}

