import { Injectable, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import {  FlightsService, Flight } from '../core';
import { catchError } from 'rxjs/operators';

@Injectable()
export class FlightResolver implements Resolve<Flight>{
    constructor(
       private flightService: FlightsService,
       private router: Router,
    ){}

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
      ): Observable<any> {
    
        return this.flightService.getBySlug([route.params['slug']]);
      }
}
