import { Component, EventEmitter, Input, Output, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
    Flight,
    FlightsService,
    CommentsService
    
  }  from '../core';
import { Comment} from '../core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-flight-comment',
    templateUrl: './flight-comment.component.html'
  })
export class FlightCommentComponent implements OnInit, OnDestroy{

    constructor(
        private route: ActivatedRoute,
        private flightService: FlightsService,
        private CommentsService: CommentsService,
        


      ) {
        
      }
      private subscription: Subscription;

      @Input() comment: Comment;
      @Output() deleteComment = new EventEmitter<boolean>();
      ngOnInit(){
        console.log(this.route.data['_value'].flight[0].id)
         this.CommentsService.getCommentsFromFlight(this.route.data['_value'].flight[0].id).subscribe(
           data =>{
             console.log(data)
             this.comment = data;
           }
         )
     

       
        
      }

      ngOnDestroy(){

      }
}