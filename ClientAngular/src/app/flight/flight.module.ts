import { NgModule } from '@angular/core';
import { SharedModule } from '../shared';
import { FlightRoutingModule } from './flight-routing.module';
import {FlightComponent} from './flight.component'
import {FlightResolver} from './flight-resolver.service';
import {FlightCommentComponent}  from './flight-comment.component';
@NgModule({
  declarations: [
    FlightComponent,
    FlightCommentComponent
  ],
  imports: [
    SharedModule,
    FlightRoutingModule,
  ],
  providers: [
    FlightResolver
  ]
})
export class FlightModule { }
