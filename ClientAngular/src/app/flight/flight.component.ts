import { Component, EventEmitter, Input, Output, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';

import { Subscription } from 'rxjs';

import {
  Flight,
  FlightsService,
  User,
  UserService,
  CommentsService,
  Comment

  
}  from '../core';
@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {
   flight: Flight;
   currentUser: User;
   commentControl = new FormControl();



  constructor(
    private route: ActivatedRoute,
     private userService: UserService,
     private router: Router,
     private CommentsService: CommentsService,
     private FlightsService : FlightsService


  ) { }


  private subscription: Subscription;

  @Input() comment: Comment;
  @Output() deleteComment = new EventEmitter<boolean>();
  canCreate: boolean;
  canModify: boolean;
  queryData = new Array;

  ngOnInit() {
    this.route.data.subscribe(
      data => {
        this.flight = data.flight[0];
        console.log(data)
      }
    )

    this.subscription = this.userService.currentUser.subscribe(
      (userData: User) => {
        console.log(userData);
        if (userData.username){
          this.canCreate = true;
        }
        if(userData.id == this.flight.user_id){
          this.canModify = true;
        }
      }
    );
    
  }

  deleteFlight(){

    switch (this.canCreate) {
      case true:

              switch (this.canModify) {
                case true:
                    this.FlightsService.deleteOne(this.flight.id).subscribe(
                      data => {
                        this.router.navigateByUrl('/');


                      }
                    )
                  break;
              case undefined:
                  alert("You must be the owner of this flight to delete it")
                  this.router.navigateByUrl('/');

                  break;
              case false:
                  alert("You must be the owner of this flight to delete it")
                  this.router.navigateByUrl('/');
    
                  break;                  
                default:


                  break;
              }
        break;
    case undefined:
            alert("You must be logged to do those actions")
         this.router.navigateByUrl('/login');

      default:
        break;
    }
  }
  addComment(){


      switch (this.canCreate) {
        case true:
            const commentBody = this.commentControl.value;

          console.log(this.flight['id']);
          const id = this.flight['id'];
          console.log(commentBody);
          
          this.queryData = [{body: commentBody, flight_id: id}];

          this.CommentsService.addComment(this.queryData).subscribe(
            data =>{
              this.ngOnInit();            }
          )
          break;
      case undefined:

           this.router.navigateByUrl('/');

        default:
          break;
      }
        }
  
}
