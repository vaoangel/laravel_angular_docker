import { NgModule } from '@angular/core';
import { SharedModule } from '../shared';
import {ProductsRoutingModule} from './products-routing.module';
import {ProductsComponent} from './products.component';

@NgModule({
    declarations: [
        ProductsComponent,
    ],
    imports: [
        ProductsRoutingModule,
        SharedModule,
    ],
    providers: [

    ]
})

export class ProductsModule{}