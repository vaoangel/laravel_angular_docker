import { Component, EventEmitter, Input, Output, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder,FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

import {
 
  User,
  UserService,
  Flight,
  FlightsService
  

  
}  from '../core';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  ProductsForm: FormGroup;
  constructor(
    private route: ActivatedRoute,
     private userService: UserService,
     private fb: FormBuilder,
     private FlightsService: FlightsService,
     private router: Router,


  ) {
    this.ProductsForm = this.fb.group({
      nombre: new FormControl(''),
      pais: new FormControl(''),
      valoracion: new FormControl(''),
      hoteles: new FormControl('')
    })
   }
   private subscription: Subscription;
   canCreate: boolean;
   queryData = new Array;


  ngOnInit() {
    this.subscription = this.userService.currentUser.subscribe(
      (userData: User) => {
        console.log(userData.username);
        if (userData.username){
          this.canCreate = true;
        }
      }
    );
  }



  submitForm(){

    console.log(this.ProductsForm.value.nombre)


    switch (this.canCreate) {
      case true:

   
        
        this.queryData = [
          {
            nombre: this.ProductsForm.value.nombre,
             pais: this.ProductsForm.value.pais, 
             valoracion: this.ProductsForm.value.valoracion,
              hoteles: this.ProductsForm.value.hoteles
            }
          ];

        this.FlightsService.addOne(this.queryData).subscribe(
          data =>{
            this.router.navigateByUrl('/');         }
        )
        break;
    case undefined:

         this.router.navigateByUrl('/');

      default:
        break;
    }
  }

}
