import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { appReducers } from './store/reducers/app.reducers';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FlightsEffects } from './store/effects/flights.effects';


import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { HomeModule } from './home/home.module';
import {
  FooterComponent,
  HeaderComponent,
  SharedModule
} from './shared';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { ReduxFlightsComponent } from './redux-flights/redux-flights.component';
import {ReduxComponent} from './containers/redux/redux.component'
// import { ProductsComponent } from './products/products.component';
// import { FlightComponent } from './flight/flight.component';
// import { FlightsComponent } from './flights/flights.component';

@NgModule({
  declarations: [AppComponent, FooterComponent, HeaderComponent,ReduxComponent,ReduxFlightsComponent],
  imports: [
    BrowserModule,
    CoreModule,
    SharedModule,
    HomeModule,
    AuthModule,
    AppRoutingModule,
    StoreModule.forRoot(appReducers),
    EffectsModule.forRoot([FlightsEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
