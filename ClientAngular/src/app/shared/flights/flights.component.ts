import { Component, OnInit } from '@angular/core';
import { FlightsService } from '../../core';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})
export class FlightsComponent implements OnInit {

  constructor(
    private FlightsService: FlightsService,
  ) { }
Flights: String;
  ngOnInit() {
    this.FlightsService.getAll().subscribe(data => {
      console.log(data);
      this.Flights = data;
    })
  }

}
