import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReduxFlightsComponent } from './redux-flights.component';

describe('ReduxFlightsComponent', () => {
  let component: ReduxFlightsComponent;
  let fixture: ComponentFixture<ReduxFlightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReduxFlightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReduxFlightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
