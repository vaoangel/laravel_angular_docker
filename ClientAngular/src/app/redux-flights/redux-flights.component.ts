import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Store, select } from '@ngrx/store';
 import  { IFlight } from '../core/models/flight.interface';
 import {selectFlightsList} from '../store/selectors/flights.selector';
 import {GetFlights} from '../store/actions/flight.actions';
 import { IAppState } from '../store/state/app.state';
 import { Router } from '@angular/router';

@Component({
  selector: 'app-redux-flights',
  templateUrl: './redux-flights.component.html',
  styleUrls: ['./redux-flights.component.css']
})
export class ReduxFlightsComponent implements OnInit {
@Input("flights")
 flights: IFlight[];
@Output()
flightSelected: EventEmitter<number> = new EventEmitter();


constructor() {}

  ngOnInit() {
//  this._store.dispatch(new GetFlights());
console.log(this.flights)
 
  }

  // navigateToFlight(id: number) {
  //   this.flightSelected.emit(id);
  // }

}
