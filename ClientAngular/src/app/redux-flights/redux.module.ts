import { NgModule } from '@angular/core';
import { SharedModule } from '../shared';
import { ReduxFlightsComponent } from './redux-flights.component';
import { ReduxRoutingModule } from './redux-routing.module';
import { ReduxComponent } from '../containers/redux/redux.component';

// @NgModule({
//     declarations: [
//         ReduxFlightsComponent,
//         ReduxComponent
        
//     ],
//     imports: [
//         SharedModule,
//         ReduxRoutingModule,
//     ],
//     providers: [

//     ]
// })

export class ReduxModule {}