import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ReduxFlightsComponent} from './redux-flights.component';

const routes: Routes = [
    {
        path: '',
        component: ReduxFlightsComponent,
        resolve: {

        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })

  export class ReduxRoutingModule {}